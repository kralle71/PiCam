#!/usr/bin/python3
"""
PiCam with tiny-yolo neuralnetwork for person detection
"""

import time
import threading
import queue
import numpy as np
import sys
import imutils
import yaml
import math
from os import system, getcwd
from keras import backend as K
from keras.models import load_model
from yad2k.models.keras_yolo import yolo_eval, yolo_head
import tensorflow as tf
import telegram_send as tele
import logging
import logging.handlers
import cv2

# load Config.yaml


def load_config():
    'load yaml config file'
    with open(getcwd()+"/config.yaml") as yamlfile:
        cfg = yaml.load(yamlfile)
    return cfg

cfg = load_config()

BASEFOLDER = cfg['base']['baseFolder']  # Webserver's base folder
# The folder which contains main script (picam.py)
SCRIPTFOLDER = cfg['base']['scriptFolder']
# Number of the camera (if -1 it will be the first camera read by the system)
CAM = cfg['input']['cam']
# Your telegram user name so all the images will be sent
# to your telegram chat with yourself
EMAIL = cfg['output']['email']
TELEGRAM_CONFIG = cfg['output']['telegram_config']


# GLOBAL VARIABLES - Initialization #
cap = None
fps_cam = 0
avg = 0
import_q = queue.Queue(maxsize=cfg['queue']['import'])
movement_q = queue.Queue(maxsize=cfg['queue']['movement'])
yolo_q = queue.Queue(maxsize=cfg['queue']['yolo'])
threads = []
W = 0  # width of outputframe
H = 0  # height of outputframe
RANGE_AREA = range(cfg['process']['min_area'], cfg['process']['max_area'])
msg_time_intervall_list = [0, math.floor(cfg['process']['msg_time_i']/4), math.floor(
    cfg['process']['msg_time_i']/2), cfg['process']['msg_time_i'], cfg['process']['msg_time_i']*4]
RESET_TIME_INTERVALL = math.floor(cfg['process']['msg_time_i']/2)

last_skip = time.time()
SKIP_TIMEOUT = cfg['process']['skip_timeout']
skipped_frames = 0


def set_up_logging():
    '''Setup for logging to syslog and some logfile	'''
    # File handler for /var/log/some.log
    serverlog = logging.FileHandler(SCRIPTFOLDER + 'picam.log')
    serverlog.setLevel(logging.WARNING)
    serverlog.setFormatter(logging.Formatter(
        '%(asctime)s %(levelname)s %(message)s'))

    # Syslog handler
    #syslog = logging.handlers.SysLogHandler(address='/dev/log')
    #syslog.setLevel(logging.DEBUG)
    #syslog.setFormatter(logging.Formatter('%(levelname)s %(message)s'))

	# Console log handler
    conlog = logging.StreamHandler(sys.stdout)
    conlog.setLevel(logging.DEBUG)
    conlog.setFormatter(logging.Formatter('[Picam] %(asctime)s %(levelname)s %(message)s'))
	

    # Combined logger used elsewhere in the script
    loggers = logging.getLogger('picam_log')
    loglevels = {'DEBUG': logging.DEBUG, 'INFO': logging.INFO, 'WARNING': logging.WARNING, 'ERROR': logging.ERROR}
    try:
    	loggers.setLevel(loglevels[cfg['base']['loglevel']])
    except KeyError as ex:
    	loggers.setLevel(logging.DEBUG)
    	print('[PiCam] set loglevel in config.yaml to one of DEBUG/INFO/WARNING/ERROR: %s' % ex)
    loggers.addHandler(serverlog)
    #loggers.addHandler(syslog)
    loggers.addHandler(conlog)
    return loggers

# send telegram text message


def tg_msg(text):
    tele.send(messages=[text], conf=TELEGRAM_CONFIG)

def skip_check(time):
    '''skip timeout to not spam the logger, while continous movement'''
    global last_skip, skipped_frames
    if (skipped_frames > 0 and math.floor(time-last_skip) > SKIP_TIMEOUT) or skipped_frames == SKIP_TIMEOUT:
        logger.warning("skipping too many frames %s" % (skipped_frames))
        skipped_frames = 0
    else:
        skipped_frames += 1
    last_skip = time

def put_q(this_q, frame):
    '''non blocking put'''
    try:
        this_q.put_nowait(frame)
    except queue.Full as ex:
        skip_check(time.time())

# when an error occur


def printExit(out):
    logger.error(out)
    exit(-1)

# Updating index.html :
#       1 Opening index.html
#       2 Adding new link for the new video
#       3 If there are more then maxDays links removing oldest one
#       4 Removing also the oldest video


def handleFile(filename):
    logger.debug("[PiCamNN] Updating file...")
    f = open(BASEFOLDER + "index.html", "r")
    cont = f.read()
    f.close()
    if cont.find(filename) != -1:
        logger.debug("[PiCamNN] File has been update yet !")
        return False
    f = open(BASEFOLDER + "index.html", "w")
    lines = cont.split("\n")
    day = 0
    go = False
    for i in range(len(lines)-1):
        if lines[i].find("<!-- S -->") != -1:
            i += 1
            f.write(
                "<!-- S -->\n <video src=\"{0}{1}\" controls muted loop>\
                </video><br>\n".format(filename, cfg['output']['container']))
            day += 1
            go = True
        elif lines[i].find("<!-- E -->") != -1:
            f.write("{}\n".format(lines[i]))
            go = False
            if day > cfg['process']['max_days']:
                rm = lines[i-3]
                rm = rm[13:28]
                try:
                    logger.info("[PiCam] remove old file %s" % (BASEFOLDER+rm))
                    system("rm {}".format(BASEFOLDER+rm))
                    logger.info("[PiCamNN] Old file removed.")
                except:
                    logger.error(
                        "[PiCamNN] An error occured while removing old file!")
        elif go:
            day += 1
            if day <= cfg['process']['max_days']:
                f.write("{}\n".format(lines[i]))
        else:
            f.write("{}\n".format(lines[i]))
    f.close()
    logger.info("[PiCamNN] index.html UPDATED")
    return True

# Some morphological operations on two input frames to check for movements


def movement(mat_1, mat_2):
    global movement_q, avg
    contours = False
    timer = math.floor(time.time())
    mat_2_gray = cv2.cvtColor(mat_2.copy(), cv2.COLOR_BGR2GRAY)
    mat_2_gray = cv2.GaussianBlur(mat_2_gray, (cfg['process']['gaussblur'], cfg['process']['gaussblur']), 0)
    mat_diff = cv2.absdiff(mat_1, mat_2_gray)
    _, mat_diff = cv2.threshold(mat_diff, 25, 255, cv2.THRESH_BINARY)
    mat_diff = cv2.dilate(mat_diff, None, iterations=2)
    _, contours, _ = cv2.findContours(
        mat_diff, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    # update the average frame
    cv2.accumulateWeighted(mat_2_gray, avg, cfg['process']['avg_ratio'])
    if contours:
        mat_2_box = mat_2.copy()
        max_contour = 0
        # if contour in range mark the frame with movement
        for c in contours:
            if cv2.contourArea(c) not in RANGE_AREA:

                continue
            # paint rectangle around movement
            (x, y, w, h) = cv2.boundingRect(c)
            cv2.rectangle(mat_2_box, (x, y), (x + w, y + h), (0, 0, 255), 2)
            max_contour = max(max_contour, round(cv2.contourArea(c) / 1000))
        # {frame, frame with box, contoursize, time}
        if max_contour > 0:
            put_q(movement_q, {'frame': mat_2, 'boxed': mat_2_box, 'contour': max_contour, 'time': timer})
            return True
    else:
        return False

# adding movement frame Thread


def addframeThread():
    global movement_q, yolo_q, writer
    t = threading.current_thread()
    last_t = math.floor(time.time())
    while True:
        try:
            event = movement_q.get()
            time.sleep(0.1)
            movement_q.task_done()
            # send the frame only every x seconds to the NN to reduce load, only if yoloThreads are greater 0
            if cfg['thread']['yoloThread'] and event['time'] - last_t >= cfg['process']['send_to_detection_i']:
                put_q(yolo_q, event.copy())
                last_t = event['time']
            # add movement frame to the video
            cv2.putText(event['boxed'],
                        str(event['contour']) + "k"
                        + " " + time.ctime(event['time']), (200, H - 50),
                        cv2.FONT_HERSHEY_PLAIN, 1, (255, 255, 255))
            writer.write(cv2.resize(event['boxed'], (W, H)))  # Adding to File
        except Exception as ex:
            tg_msg("[PiCam_%s] Just died" % t.name)
            logger.error("Some error occured : %s" % ex)
            # sys.exit(-1)
            return

# Movement checking Thread


def moveThread():
    global import_q, movement_q, avg
    t = threading.current_thread()
    start_time = end_time = math.floor(time.time() / 60)
    str_start_time = time.strftime('%H:%M')
    moving = False

    while True:
        try:
            b = import_q.get()
            time.sleep(0.1)
            import_q.task_done()
            move = movement(cv2.convertScaleAbs(avg), b)
            # if we have got a movement, print report to log
            if move:
                # check if movement lasts at least 2 min to report to log
                if not moving:
                    start_time = math.floor(time.time() / 60)
                    end_time = math.floor(time.time() / 60)
                    str_start_time = time.strftime("%H:%M")
                    moving = True
                else:
                    end_time = math.floor(time.time() / 60)
            if moving and math.floor(time.time() / 60) - end_time > 2:
                logger.info("[PiCam_%s] Movement (%s - %s)" %
                            (t.name, str_start_time, time.strftime("%H:%M")))
                moving = False
        except Exception as ex:
            tg_msg("[PiCam_%s] Just died" % t.name)
            logger.error("Some error occured : %s" % ex)
            # sys.exit(-1)
            return

# Frame reading Thread


def frameThread():
    global cap, import_q
    t = threading.current_thread()
    frame_count = 0
    while True:
        try:
            _, a = cap.read()
            if _ is False:
                for i in range(cfg['process']['frame_check']):
                    cap = cv2.VideoCapture(CAM)
                    _, a = cap.read()
                    if _ is True:
                        break
                    time.sleep(15)
                    if i == cfg['process']['frame_check']-1:
                        printExit(
                            "[PiCam_%s] Error with camera stream!" % t.name)
            if frame_count % round(fps_cam / cfg['input']['fps']) == 0:
                if cfg['process']['resize_width'] != 0:
                    a = imutils.resize(a, width=abs(cfg['process']['resize_width']))
                put_q(import_q,a)
            frame_count += 1
        except Exception as ex:
            tg_msg("[PiCam_%s] Just died" % t.name)
            logger.error("Some error occured : %s" % ex)
            return

# Pedestrian Recognition Thread


def yoloThread():
    global yolo_q
    t = threading.current_thread()
    model_path = SCRIPTFOLDER + "tiny.h5"  # Model weights
    sess = K.get_session()
    logger.debug("[PiCam_%s] Loading anchors file..." % t.name)
    anchors = [1.08, 1.19, 3.42, 4.41, 6.63, 11.38, 9.42,
               5.11, 16.62, 10.52]  # Tiny Yolo anchors' values
    anchors = np.array(anchors).reshape(-1, 2)
    logger.debug("[PiCam_%s] Loading yolo model (%s)..." %
                 (t.name, SCRIPTFOLDER + "tiny.h5"))
    yolo_model = load_model(model_path)  # Loading Tiny YOLO
    #num_anchors = len(anchors)
    logger.debug('[PiCam_%s] YOLO model loaded (%s) !' % (t.name, model_path))

    model_image_size = yolo_model.layers[0].input_shape[1:3]  # Get input shape
    yolo_outputs = yolo_head(yolo_model.output, anchors, 20)
    input_image_shape = K.placeholder(shape=(2, ))
    boxes, scores, classes = yolo_eval(
        yolo_outputs, input_image_shape, score_threshold=0.3, iou_threshold=0.4)
    num = 0  # Starting Photo's name
    first_time = last_time = math.floor(time.time())
    LIST_ITER = 0

    logger.info("[PiCam_%s] YOLO Thread started!" % t.name)
# Loop:
    while True:
        event = yolo_q.get()
        time.sleep(0.1)
        yolo_q.task_done()
        mat = event['frame']
        try:
            mat = cv2.resize(mat, (model_image_size[0], model_image_size[1]))
            in_mat = np.array(mat, dtype='float32')
            in_mat /= 255.  # Removing mean
            in_mat = np.expand_dims(in_mat, 0)
            # Searching for detection:
            out_boxes, out_scores, out_classes = sess.run([boxes, scores, classes], feed_dict={
                                                          yolo_model.input: in_mat, input_image_shape: [mat.shape[1], mat.shape[0]], K.learning_phase(): 0})
            if len(out_boxes) > 0:
                writ = False
                xs, ys = [], []  # X's and Y's coordinate
                for i, c in reversed(list(enumerate(out_classes))):
                    if c == 14:  # 14 is the label for persons
                        writ = True
                        box = out_boxes[i]
                        top, left, bottom, right = box
                        top = max(0, np.floor(top + 0.5).astype('int32'))
                        left = max(0, np.floor(left + 0.5).astype('int32'))
                        bottom = min(mat.shape[1], np.floor(
                            bottom + 0.5).astype('int32'))
                        right = min(mat.shape[0], np.floor(
                            right + 0.5).astype('int32'))
                        xs.append(left+i)
                        xs.append(right-i)
                        ys.append(top+i)
                        ys.append(bottom-i)
                if writ:
                    img_name = SCRIPTFOLDER+"imgs/{}.png".format(num)
                    cv2.imwrite(img_name, event['boxed'])
                    out_s = "[{}] Detected person (taken {}s)! contoursize = {}k\n".format(time.strftime(
                        "%H:%M:%S"), math.floor(time.time())-event['time'], event['contour'])  # Log output
                    logger.info(out_s)
                    # reset continious person movement after x minutes
                    if math.floor((event['time'] - last_time) / 60) > RESET_TIME_INTERVALL:
                        LIST_ITER = 0
                        first_time = event['time']
                    # reduce photo sending with msg_time_intervall_list
                    if math.floor((event['time'] - first_time) / 60) >= msg_time_intervall_list[LIST_ITER]:
                        try:  # send photo with telegram
                            with open(img_name, "rb") as f:
                                tele.send(images=[f], conf=TELEGRAM_CONFIG)
                            if LIST_ITER < len(msg_time_intervall_list) - 1:
                                LIST_ITER += 1
                            # send photo with email
                            #subprocess.call('mpack -s "motion detected" {} {}'.format(img_name,email),timeout=30,shell=True)
                        except Exception as exc:
                            tg_msg("[PiCam_{}] Just died".format(t.name))
                            logger.error(
                                "[PiCam] Couldn't send photo: %s" % exc)
                    num += 1
                    last_time = event['time']  # Updating last detection time
        except Exception as ex:
            tg_msg("[PiCam_{}] Just died".format(t.name))
            logger.error(
                "[PiCam_%s] Some error occured in YOLO Thread: %s" % (t.name, ex))
            return
        
    # start n threads of a given function


def start_thread(targetfunc, func_name, number_threads):
    global threads
    for i in range(0, number_threads):
        t = threading.Thread(
            target=targetfunc, name="{}_{}".format(func_name, i+1))
        threads.append(t)
        t.start()
        logger.info("[PiCam] Starting %s %s/%s...." %
                    (func_name, i+1, number_threads))


def thread_monitor():
    global threads, cap
    # start image processing threads
    start_thread(yoloThread, 'yoloThread', cfg['thread']['yoloThread'])
    start_thread(frameThread, 'frameThread', cfg['thread']['frameThread'])
    start_thread(moveThread, 'moveThread', cfg['thread']['moveThread'])
    start_thread(addframeThread, 'addframeThread',
                 cfg['thread']['addframeThread'])

    threads_started = threading.activeCount()
    # check health

    while True:
        if math.floor(time.time() / 60) % 10 == 0:  # every 10 minutes check health
            logger.debug("[PiCam] Checking Health")
            for t in threads:
                if t.isAlive() is False:
                    # restart if down
                    threads.remove(t)
                    name, _ = t.name.split('_')
                    if name == 'yoloThread':
                        s = threading.Thread(target=yoloThread, name=t.name)
                    elif name == 'frameThread':
                        cap = cv2.VideoCapture(CAM)
                        s = threading.Thread(target=frameThread, name=t.name)
                    elif name == 'moveThread':
                        s = threading.Thread(target=moveThread, name=t.name)
                    elif name == 'addframeThread':
                        s = threading.Thread(
                            target=addframeThread, name=t.name)
                    threads.append(s)
                    s.start()
                    logger.warning("[PiCam] restart thread %s" % s.name)
                    tg_msg("[PiCam] restart thread {} ({})".format(
                        s.name, time.strftime("%H:%M:%S")))

        if math.floor(time.time() / 60) % 60 == 0:  # every hour print status
            if import_q.qsize() + movement_q.qsize() + yolo_q.qsize() > 0:
                logger.warning("[PiCam] frameQ: %s | moveQ: %s | yoloQ: %s" % (
                    import_q.qsize(), movement_q.qsize(), yolo_q.qsize()))
            if threading.activeCount() < threads_started:
                logger.warning("[PiCam] %s Threads are active" %
                               threading.activeCount())
        time.sleep(60)


'''
Main code from here :
'''

if __name__ == "__main__":
    logger = set_up_logging()
    logger.info("Starting PiCam....")
    #tg_msg("Starting PiCam...")
    filename = time.strftime("%c").replace(" ", "_")[0:10]
# Camera Input
    cap = None
    try:
        cap = cv2.VideoCapture(CAM)  # Trying to open camera
        _, dim = cap.read()
        if not _ or dim.shape == (0, 0, 0):
            printExit("[PiCam] Error occured when opening the camera stream! (%s,%s)" % (_, dim))
        # initial average with first frame
        fps_cam = cap.get(5)/10000  # fps of camera stream
        logger.debug("[PiCam] fps provided by camera: %s" % fps_cam)
        if cfg['input']['fps'] > fps_cam:
            printExit(
                'fps_input is higher than max fps provided by cam (= %s fps)' % fps_cam)
        if cfg['process']['resize_width'] != 0:
            dim = imutils.resize(dim, width=abs(cfg['process']['resize_width']))
        avg = cv2.cvtColor(dim.copy(), cv2.COLOR_BGR2GRAY)
        avg = cv2.GaussianBlur(avg, (cfg['process']['gaussblur'], cfg['process']['gaussblur']), 0)
        avg = avg.copy().astype('float')
        H = dim.shape[0]
        W = dim.shape[1]
        logger.debug("[PiCam] Height: %s | Width: %s" % (H, W))
    except:
        printExit("[PiCam] Error occured when opening the camera stream!")
# Video Output
    writer = None
    err = "[PiCam] Error occured when opening the output video stream!"
    load = handleFile(filename)  # Updating web_file
    if not load:
        system("mv {0}{1} {0}_{1}".format(BASEFOLDER + filename, cfg['output']['container']))
    try:
        writer = cv2.VideoWriter(BASEFOLDER + filename + cfg['output']['container'],
                             cv2.VideoWriter_fourcc(*cfg['output']['fourcc']), cfg['output']['fps'], (W, H), True)
    except Exception as ex:
        logger.debug("Problem with video: %s" % ex)
    if not writer.isOpened():
        printExit(err)  # If output Stream is unavailable
# Loading video file of the same day:
    if not load:
        try:
            logger.info("[PiCam] Loading old video File...")
            read = cv2.VideoCapture(BASEFOLDER+filename+"_"+cfg['output']['container'])
            _, mat = read.read()
            while _:
                if mat.shape == (0, 0, 0) or mat.shape[0] != H or mat.shape[1] != W:
                    logger.error(
                        "[PiCam] Couldn't load old file skipping(shape %s)...!" % mat.shape)
                    break
                writer.write(mat)
                _, mat = read.read()
            del read, mat
            logger.debug("loaded!")
        except:
            logger.error("\n[PiCam] Couldn't load old file skipping...!")
        logger.debug("[PiCam]Remove old video file %s_.%s" %
                     (BASEFOLDER + filename, cfg['output']['container']))
        # Removing old video file
        try:
            system("rm {}_{}".format(BASEFOLDER + filename, cfg['output']['container']))
        except:
            logger.debug("Couldn't remove %s" % (BASEFOLDER + filename + "_" + cfg['output']['container']))

    monitoring_thread = threading.Thread(
        target=thread_monitor, name="thread_monitor")
    monitoring_thread.start()
    logger.info("[PiCam] Monitoring thread is started")
    tg_msg('Security Cam activated')
    logger.warning('[PiCam] Security Cam activated')

    day = time.strftime("%d")  # startin' day
# Loop's start
    logger.info("[PiCam] Starting main loop...")
    while True:
        try:  # print status of the queues and threads
            # reload config every x minutes
            if int(time.strftime("%M")) % cfg['process']['reload_config'] == 0:
                cfg = load_config()

            if time.strftime("%d") != day:
                writer.release()  # Closing old video output
                logger.debug("[PiCam] Cleaning imgs dir...")
                try:
                    system("rm {}".format(SCRIPTFOLDER+"imgs/*"))
                except Exception as ex:
                    logger.debug("Couldn't delete files from %s . No Images found." % (SCRIPTFOLDER+"imgs/*"))
                logger.info("[PiCam] New day! Restarting video output....")
                filename = time.strftime("%c").replace(" ", "_")[0:10]
                cfg = load_config() # reload config
                try:
                    writer = cv2.VideoWriter(
                        BASEFOLDER + filename + cfg['output']['container'], cv2.VideoWriter_fourcc(*cfg['output']['fourcc']), cfg['output']['fps'], (W, H), True)
                except Exception as ex:
                    logger.debug("Problem with video: %s" % ex)
                logger.debug("[PiCam] Updating index.html...")
                handleFile(filename)
                day = time.strftime("%d")
                logger.debug("[PiCam] Done! Resuming main loop...")
            time.sleep(3600)
        except Exception as ex:
            logger.error("Some error occured : %s" % ex)
            sys.exit(-1)
