FROM jermine/opencv:aarch64-ubuntu-3.4.1-tensorflow1.5
MAINTAINER kralle71
RUN groupadd -r picam && useradd -g picam -s /bin/bash picam
RUN apt-get update && apt-get install -y \
	nano \
	build-essential \
	libssl-dev \
	libffi-dev \
	python-dev \
	libsm6 \
	libxrender-dev \
	libxext6 \
	libhdf5-dev && \
	apt-get clean && \ 
	pip3 install \
	keras \
	telegram_send \
	pyyaml	
WORKDIR /app
COPY . /app
RUN mkdir /www && \
	chown -R picam:picam /www /app && \
	chmod -R 755 /www /app
USER picam
CMD ["python3", "picam.py"]
