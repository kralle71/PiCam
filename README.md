#
# PiCam-NN
## with neural network person detection

Thanks and credit goes to [PiSimo](https://github.com/PiSimo/PiCamNN "github.com/PiSimo/PiCamNN")
who created this project. 
My version is just an evolution of his great project to satisfy my needs.

PiCam-NN is a self-made surveillance system, designed to run on a Raspberry Pi 3B.

PiCam-NN will detect persons with [tiny_yolo_v2](https://pjreddie.com/darknet/yolo/), a deep learning neural network.

The Project is kinda past the target. You can simply use 
[motion](https://github.com/Motion-Project/motion "github.com/Motion-Project/motion")
, but PiCam-NN provides way better movement detection
and in addition an AI to tell you if a person 
was detected.

The pretrained nn-model will detect:
```python
voc_classes = [
    "aeroplane", "bicycle", "bird", "boat", "bottle", "bus", "car", "cat",
    "chair", "cow", "diningtable", "dog", "horse", "motorbike", "person",
    "pottedplant", "sheep", "sofa", "train", "tvmonitor"
]
```
Feel free to train your own model, or use other pretrained models.

__How cool is that!__

What you need:
* Server (Pi will do)
* Outdoor IP Cam with RTSP Stream (USB-Webcam will do)
* Python3
    * opencv v3
    * tensorflow
* Webserver (Apache/Nginx)
* Telegram Bot (or Email)

_My Setup (Raspberry Pi 3B, Outdoor IP Cam with RTSP Stream)
runs with 1 FPS to reduce CPU load of the Pi. 4 FPS with Pi3B is possible,
but will result in 100% load 24/7. For my task 1 FPS is sufficient.
If you need more FPS, you need a more powerful server._

#### added features:

* microservices
    * tasks are threaded as microservices
    * monitored and self healing microservices
    * you can easily scale threads (-> FPS), according to load and CPU-power

* configuration
    * change config while running
    * configure how often you want a push message
    * configure how often you to check for detections (to reduce load)

* movement detection
    * improved movement detection
    * configure thresholds of movement detection
    * improved contour finding
    * frame averaging to reduce shadow-movement-events



![alt text](dataflow.jpg?raw=true "Dataflow")

## installation instructions

```bash
git clone https://gitlab.com/kralle71/PiCam.git
```
```bash
cd PiCam && mkdir imgs
```
download the tiny yolo weights:
```bash
wget https://www.dropbox.com/s/xastcd4c0dv2kty/tiny.h5?dl=0 -O tiny.h5
```
move index.html to your webserver's basefolder
```bash
sudo mv index.html /var/www/html/
```
configure PiCam with preferred editor
```bash
mv config_template.yaml config.yaml
```
```bash
nano config.yaml
```
install python3 requirements
```bash
sudo pip3 install -r requirements.txt
```